## ARGlass ##

Sistema interactivo  para la identificacion de objetos dentro de una residencia, por medio de sonidos como medio de apoyo a personas invidentes utilizando **Realidad Aumentada.**

### Requisitos ###
- [Flex SDK ](http://) Version 3.2.x o superior
- [FLARManager](http://)
- [Flash Player] () version 11.x o superior
- Sistemas Operativos: Windows XP SP2 o superior (ej. Windows 7, Windows 8), Gnu/Linux, Mac OSX

### Instalación ###
Clonar este repositorio y alojarlo en una carpeta conveniente

	git clone git://github.com/diniremix/Radio21.git
	

### Licencia ###
[LGPL-2.1](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html)

El texto completo de la licencia puede ser encontrado en el archivo **LGPL.txt**

### Contacto ###
[Diniremix on Github](https://github.com/diniremix)

email: *diniremix [at] gmail [dot] com*
