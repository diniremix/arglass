//ARGlass
/*
package {
	import flash.display.Sprite;

	public class ARGlass extends Sprite
	{
		public function ARGlass()
		{
		}
	}
}
*/


package {
	import com.transmote.flar.FLARManager;
	import com.transmote.flar.marker.FLARMarker;
	import com.transmote.flar.marker.FLARMarkerEvent;
	import com.transmote.flar.utils.geom.FLARPVGeomUtils;
	
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.errors.*;
	import flash.events.*;
	import flash.geom.Point;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.*;
	
	import org.libspark.flartoolkit.support.pv3d.FLARCamera3D;
	import org.papervision3d.core.math.Matrix3D;
	import org.papervision3d.core.math.Number3D;
	import org.papervision3d.lights.PointLight3D;
	import org.papervision3d.materials.BitmapMaterial;
	import org.papervision3d.materials.utils.MaterialsList;
	import org.papervision3d.objects.DisplayObject3D;
	import org.papervision3d.objects.parsers.DAE;
	import org.papervision3d.render.LazyRenderEngine;
	import org.papervision3d.scenes.Scene3D;
	import org.papervision3d.view.Viewport3D;
		
	[SWF(width="640", height="480", frameRate="30", backgroundColor="#000000")]

	public class ARGlass extends Sprite	{
		[Embed(source="sonidos/Nest_-_05_-_Cad_Goddeu.mp3.MP3")]
		private var musica:Class;		
		[Embed(source="sonidos/buzz.mp3")]
		private var buzz:Class;				

		[Embed(source="modelos/caja.dae", mimeType="application/octet-stream")]
		private var Caja:Class;
		[Embed(source="imagenes/metalico.jpg")]
		private var skin1_1:Class;
		[Embed(source="imagenes/metalutem.jpg")]
		private var skin1_2:Class;
		
		[Embed(source="modelos/grua.dae", mimeType="application/octet-stream")]
		private var Iman:Class;	
		[Embed(source="imagenes/grua.jpg")]
		private var skin2:Class;
		//
		[Embed(source="modelos/tierra.dae", mimeType="application/octet-stream")]
		private var Tierra:Class;
		[Embed(source="imagenes/shadow.png")]
		private var skin3_1:Class;
		[Embed(source="imagenes/800px-Earthmap1000x500.jpg")]
		private var skin3_2:Class;

		[Embed(source="modelos/scout.dae", mimeType="application/octet-stream")]
		private var Vaca:Class;
		[Embed(source="imagenes/scout_flat.jpg")]
		private var skin4:Class;
		//
		
		[Embed(source="imagenes/volver1.png")]	private var volver1:Class ;
		[Embed(source="imagenes/volver2.png")]	private var volver2:Class ;
		
		[Embed(source="imagenes/NOcam.jpg")]
		private var no_cam:Class;
		private var noCam:Bitmap=new Bitmap(new no_cam().bitmapData);		
						
		private var txt:TextField = new TextField();
		private var volver_1:Bitmap=new Bitmap(new volver1().bitmapData); 
		private var volver_2:Bitmap=new Bitmap(new volver2().bitmapData);
		private var myButton:SimpleButton = new SimpleButton();
		
		private var fm:FLARManager;
		private var scene3D:Scene3D;
		private var viewport3D:Viewport3D;
		private var camera3D:FLARCamera3D;
		private var lre:LazyRenderEngine;
		private var pointLight3D:PointLight3D;
		
		//Objetos DAE a Cargar
		private var caja:DAE;		
		private var iman:DAE;
		private var tierra:DAE;	
		private var vaca:DAE;	
		
		/*Se declaran 2 markers. Si se requiriera utilizar mas Markers, lo ideal
		Sería crear un array de markers en vez de declararlos individualmente, pero
		el programa se complicaría un poco mas (pero se haría mas corto)*/	
		private var marker1:FLARMarker;
		private var marker2:FLARMarker;
		private var marker3:FLARMarker;			
		private var marker4:FLARMarker;			

		/*El siguiente array posee contenedores,cada indice indica con cual marker
		serán asociados. En este caso serán utilizados solo 2 markers, asi que solo se crean
		2 elementos en el array.*/	
		private var contenedor_objetos:Array=new Array(new DisplayObject3D,new DisplayObject3D,new DisplayObject3D,new DisplayObject3D);
		
		private var Musica:Sound;
		private var Buzz:Sound;
		
		private var SW:int=0;
		private var sw:int=0;
		private var dibujos:Shape;
		
		
		public function ARGlass() {
			var mats1:MaterialsList = new MaterialsList();
			mats1.addMaterial(new BitmapMaterial(new skin1_1().bitmapData), "Material__0");
			mats1.addMaterial(new BitmapMaterial(new skin1_2().bitmapData), "Material__1");
						
			caja = new DAE();
			caja.load(new Caja(),mats1);
			caja.scale = 6;
			caja.rotationX = 90;

			var mats2:MaterialsList = new MaterialsList();
			mats2.addMaterial(new BitmapMaterial(new skin2().bitmapData), "_2_-_Default");						
			
			iman = new DAE();
			iman.load(new Iman(),mats2);
			iman.scale = 7;
			iman.rotationX = 90;
			iman.rotationZ = 180;
			
			//
			var mats3:MaterialsList = new MaterialsList();
			mats3.addMaterial(new BitmapMaterial(new skin3_1().bitmapData), "shadow_png");
			mats3.addMaterial(new BitmapMaterial(new skin3_2().bitmapData), "i800px-Earthmap100");						
			
			tierra = new DAE();
			tierra.load(new Tierra(),mats3);
			tierra.scale = 4;
			tierra.rotationX = 90;
			tierra.rotationZ = 180;

			var mats4:MaterialsList = new MaterialsList();
			mats4.addMaterial(new BitmapMaterial(new skin4().bitmapData), "scout_red");						
			
			vaca = new DAE();
			vaca.load(new Vaca(),mats4);
			vaca.scale = 0.5;
			vaca.rotationX = 90;
			//
			
			/*
			Se asignan los objetos a los contenedores respectivos.
			Si se utilizaran más markers, y se requiriera cargar mas
			objetos, de todas maneras esto tendrá que hacerce uno por
			uno a mano, igual que aquí. Se ponen sin visibilidad para que
			no se vean si el Marker no es detectado.
			*/
			this.contenedor_objetos[0].addChild(iman);
			this.contenedor_objetos[1].addChild(caja);
			this.contenedor_objetos[2].addChild(tierra);
			this.contenedor_objetos[3].addChild(vaca);
			this.contenedor_objetos[0].visible=false;
			this.contenedor_objetos[1].visible=false;
			this.contenedor_objetos[2].visible=false;
			this.contenedor_objetos[3].visible=false;
			
			Musica = new musica() as Sound;
			Buzz = new buzz() as Sound;
			
			initFLAR();
		}
		

		private function initFLAR():void {
			this.fm = new FLARManager("data/flarConfig.xml");
			//this.fm.thresholdSourceDisplay=true;
			fm.addEventListener(FLARMarkerEvent.MARKER_ADDED, onAdded);
			fm.addEventListener(FLARMarkerEvent.MARKER_REMOVED, onRemoved);
			fm.addEventListener(Event.INIT, onFlarManagerLoad);				
			this.addChild(Sprite(fm.flarSource));			
		}
		

		private function onFlarManagerLoad(e:Event):void {			
			this.fm.removeEventListener(Event.INIT, onFlarManagerLoad);	

			txt.x=5;
			txt.y=5;
			txt.autoSize=TextFieldAutoSize.LEFT;
		
		    var format:TextFormat	= new TextFormat();
	        format.size = 13;
	        format.color=0xF94607;
	        format.bold=true;
	        txt.defaultTextFormat=format;

			if(this.fm.flarCameraSource.cameraIndex>=0){
				this.fm.flarCameraSource.camera.addEventListener(StatusEvent.STATUS, onCameraStatus);				
				this.initPaperVision();
			    txt.text="Creado por José Padilla M.\nEscuela de Informática\nIngeniería Civil en Computación\nUTEM - Chile - 2010"     
				addChild(txt);
				
				myButton.upState = volver_1;
				myButton.overState = volver_2;
				myButton.downState = volver_2;
				myButton.hitTestState = volver_2;
				
				myButton.x=580;
				myButton.y=5;			
				myButton.addEventListener(MouseEvent.CLICK, click,false, 0, true);
				addChild(myButton);
				Musica.play(0,999,null);						
		 	}else{
		 		txt.opaqueBackground=true;
				addChild(noCam); 
		        txt.text="ERROR: No se detectó cámara web conectada en su equipo.\nUna vez solucionado su problema recargue la página.";     
			    addChild(txt);
		  }	  
		}
		
		
		private function onAdded(e:FLARMarkerEvent):void {
			trace("añadido: " + e.marker.patternId);
			/*
			Según que marker se detecta, se hace visible, y se asigna el marker
			detectado a las variables markers globales. La idea en caso de usar
			más markers, lo ideal sería crear un ciclo while para rellenar un
			arreglo de markers segun el indice que se haya detectado.
			*/
			this.contenedor_objetos[e.marker.patternId].visible=true;	
			if(e.marker.patternId==0){
				marker1=e.marker;
			}
			if(e.marker.patternId==1){
				marker2=e.marker;				
			}
			if(e.marker.patternId==2){
				marker3=e.marker;				
			}
			if(e.marker.patternId==3){
				marker4=e.marker;				
			}					
		}


		private function onRemoved(e:FLARMarkerEvent):void {
			trace("removido: " + e.marker.patternId);			
			/*
			Si se deja de detectar un marker, la variable global que
			contenia el marker se pone nula, y el objeto se hace invisible.
			*/			
			this.contenedor_objetos[e.marker.patternId].visible=false;
			if(e.marker.patternId==0){
				marker1=null;				
			}
			if(e.marker.patternId==1){
				marker2=null;					
			}
			if(e.marker.patternId==2){
				marker3=null;					
			}
			if(e.marker.patternId==3){
				marker4=null;					
			}				
		}


		private function initPaperVision():void {
			this.scene3D = new Scene3D();
			this.camera3D = new FLARCamera3D(this.fm.cameraParams);
			this.viewport3D = new Viewport3D(640, 480, true);
			this.addChild(this.viewport3D);
			this.pointLight3D = new PointLight3D(true, false);

			this.pointLight3D.x = 1000;
			this.pointLight3D.y = 1000;
			this.pointLight3D.z = -1000;

			this.scene3D.addChild(pointLight3D);
			this.lre = new LazyRenderEngine(this.scene3D, this.camera3D, this.viewport3D);
			this.addEventListener(Event.ENTER_FRAME, this.onEnterFrame);
						
			this.scene3D.addChild(contenedor_objetos[0]);
			this.scene3D.addChild(contenedor_objetos[1]);
			this.scene3D.addChild(contenedor_objetos[2]);		
			this.scene3D.addChild(contenedor_objetos[3]);		
		}
		
		/*
		Esta funcion viene a reemplazar al "markerUpdated", ya que se necesitan
		hacer cosas con todos los markers detectados, y no individualmente como es
		el caso de la funcion "markerUpdated".
		*/		
		private function onEnterFrame(e:Event):void {
			var dist:Number;
			var atrac:Number=10;
			var inverse:Matrix3D = new Matrix3D();
			var parentMatrix:Matrix3D = new Matrix3D();
			
			if(dibujos)dibujos.graphics.clear();//Se limpian los dibujos hechos con la api gráfica

			/*
			Si se detecta el marker 1, simplemente se actualiza su matriz de 
			transformacion segun su marker. Para el tipo de objetos "fijos" y en caso
			de utilizar multiples markers, esto podría hacerse en un ciclo while.
			*/
			if(marker1){			
				this.contenedor_objetos[0].transform=FLARPVGeomUtils.convertFLARMatrixToPVMatrix(marker1.transformMatrix);
			}
			if(marker3){			
				this.contenedor_objetos[2].transform=FLARPVGeomUtils.convertFLARMatrixToPVMatrix(marker3.transformMatrix);
			}
			if(marker4){			
				this.contenedor_objetos[3].transform=FLARPVGeomUtils.convertFLARMatrixToPVMatrix(marker4.transformMatrix);
			}
			
			/*
			Manejo del marker2. Se actualiza la matriz de transformación, y luego 
			según eso se programa todo lo necesario para interactuar entre markers
			*/			
			if(marker2) {
				this.contenedor_objetos[1].transform=FLARPVGeomUtils.convertFLARMatrixToPVMatrix(marker2.transformMatrix);				

				//Si se han detectado los 2 markers en la escena
				if(marker1 && marker2){
					
					/*
					Se calcula la posición de la caja en la escena, y luego según esto,
					se calcula la distancia entre la caja y el marker1 utilizando
					la tipica formula de la distancia Euclidiana.
					*/
					var pos_caja:Number3D= real_scr_pos(caja,viewport3D);
					dist=Math.sqrt(Math.pow((marker1.x-pos_caja.x),2)+Math.pow((marker1.y-pos_caja.y),2)+Math.pow((marker1.z-pos_caja.z),2));
					//trace("distancia : " + dist);
					
					/*Si el marker1 esta cerca de la caja, y el marker1 apunta a la caja,
					entonces Marcar como OK para iniciar acciones.*/
					if (dist>=80 && dist<=165 && Marker_apunta_Objeto(marker1,marker2,caja,viewport3D)){
						sw=1;
						Buzz.play(0,1,null); 																
					}  
				}
				
				//Manejo de los distintos eventos que pueden suceder	
				if (caja.z < 50 && marker1 && SW==0 && sw==1){ //Estan el iman y la caja?
					caja.z+=atrac; //la caja sube
				}else{
					if(caja.z >= 50 && marker1){ //se ha llegado al tope de altura?
						if(SW==0){
							caja.z=50;
							/*Si se ha llegado al tope de la altura, se cambia la caja al contenedor
							en el marker1 (por lo tanto se debe intercambiar la matriz de transformación
							entre contenedores). Esto solo se debe hacer 1 vez al alcanzar el tope
							por eso el SW==0*/
							inverse.calculateInverse(contenedor_objetos[0].world);								
							parentMatrix.calculateMultiply(inverse, caja.world);
							this.contenedor_objetos[1].removeChild(caja);	
							caja.copyTransform(parentMatrix);
							this.contenedor_objetos[0].addChild(caja);
							SW=1;							
						}
						sw=0;
					}else{					
						if((!marker1) && caja.z>0 ){//no esta el iman y la caja se encuentra en el aire
							caja.z-=atrac;//La caja Baja
							if(caja.z<0) caja.z=0;
							if(SW==1){
								/*Si la caja estaba en el marker1, Se hace lo inverso que 
								cuando se llega al tope. Se intercambian nuevamente las matrices 
								de transformación, y la caja vuelve al contenedor del marker2. 
								Esto tambíen solo se hace 1 vez.*/
								inverse.calculateInverse(contenedor_objetos[1].world);								
								parentMatrix.calculateMultiply(inverse, caja.world);
								this.contenedor_objetos[0].removeChild(caja);	
								caja.copyTransform(parentMatrix);
								this.contenedor_objetos[1].addChild(caja);
								SW=0;						
							}
							sw=0;
						}
					}
				}			
			}else{//Desaparece la Caja
				caja.z=0;//la caja se baja a nivel del suelo.				
				if(SW==1){
					/*Si la caja estaba en el marker1 La caja se devuelve al 
					contenedor en el marker2, intercambiando denuevo las matrices
					de transformación. Nuevamente Esto se hace 1 sola vez.*/
					inverse.calculateInverse(contenedor_objetos[1].world);													
					parentMatrix.calculateMultiply(inverse, caja.world);
					this.contenedor_objetos[0].removeChild(caja);	
					caja.copyTransform(parentMatrix);
					this.contenedor_objetos[1].addChild(caja);
					SW=0;
					sw=0;								
				}			
			}
			this.lre.render();
		}
		
		
		/*
		Funcion que Permite saber si un Marker apunta a un objeto que se encuentra en otro marker
		- m1 y m2 son los Markers (donde m1 apunta a m2). La parte de Abajo del Marker es la "punta".
		- obj es el objeto dentro de m2 que será apuntado
		- Viewport es el viewport actual
		*/
		private function Marker_apunta_Objeto(m1:FLARMarker,m2:FLARMarker,obj:DisplayObject3D,viewport:Viewport3D):Boolean{
			var corners:Vector.<Point>;
			var vertex:Point;			
			var i1:int=-1;
			var i2:int=-1;
			var direccion:int=-1;
			var c:int;
			var retorno:Boolean=false;						
			
			if(dibujos) dibujos.graphics.clear();
			dibujos = new Shape();			
			
			//Calculo de orientación del marker. Segun la rotación
			//del ejeZ se determina hacia que lado esta apuntando
			if((m1.rotationZ > 315 && m1.rotationZ < 360) || (m1.rotationZ <= 45 && m1.rotationZ >= 0)){
				direccion=0;//Arriba
			}
			if(m1.rotationZ > 225 && m1.rotationZ<= 315){
				direccion=1;//Izquierda
			}
			if(m1.rotationZ > 135 && m1.rotationZ<= 225){
				direccion=2;//Abajo
			}										
			if(m1.rotationZ > 45 && m1.rotationZ<= 130){
				direccion=3;//Derecha
			}
			
			/*
			Calculos de esquinas según la orientación
			Segun la orientación se calculan cuales 2 puntos
			se encuentran en esa dirección.
			*/
			corners = m1.corners;
			switch(direccion){
				case 0: //arriba	
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.y >= vertex.y){
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;							
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}					
					break;
				case 1: //izquierda
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.x >= vertex.x){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);					
						}
					}
					break;
				case 2: //abajo
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.y < vertex.y){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}
					break;
				case 3: //derecha
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.x < vertex.x){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}	
					break;					
			}
	
			//Si se han encontrado las 2 esquinas orientadas
			if (i1>=0 && i2>=0){
				/*
				Calculo del punto medio entre las 2 esquinas seleccionadas.
				Si en la imagen se ve correctamente según la orientación
				el punto azul, entonces se detectó correctamente la orientación
				*/
				var p3:Point=Point.interpolate(corners[i1],corners[i2],0.5);
				
				//Calculo de la posicion 2D del objeto en el marker 2
				var pos_obj:Number3D= real_scr_pos(obj,viewport);
				
				//Dibuja el punto p3
				//dibujos.graphics.lineStyle(3, 0x0000FF);
				//dibujos.graphics.drawCircle(p3.x, p3.y, 5);
				
				//Dibuja el punto central del marker1
				//dibujos.graphics.lineStyle(3, 0xFF0000);
				//dibujos.graphics.drawCircle(m1.x,m1.y,3);
				
				//Dibuja el punto central del objeto en el marker2
				//dibujos.graphics.lineStyle(3, 0xFF0000);
				//dibujos.graphics.drawCircle(pos_obj.x,pos_obj.y,3);
				
				//Dibuja linea entre p3 y el objeto del marker2
				//dibujos.graphics.moveTo(p3.x,p3.y);
				//dibujos.graphics.lineTo(pos_ojb.x,pos_ojb.y);
				
				//dibuja linea entre el marker1 y el objeto del marker2
				//dibujos.graphics.lineStyle(3, 0xFFD700);
				//dibujos.graphics.lineTo(m1.x,m1.y);

				/*se calcula si p3 pertenece a la ecuacion de la recta que pasa por el marker1 y 
				el objeto del marker2. La idea es que esta diferencia sea la menor posible. Se 
				recomiendan valores entre 0 y 10. Si se cumple la condición, el marker1 si se 
				encuentra apuntando al objeto del marker2*/	
				var r:Number=Math.abs((m1.y+(pos_obj.y - m1.y)*(p3.x - m1.x)/(pos_obj.x - m1.x))-p3.y);
				trace("r: " + r);
				if(r<10){
					retorno=true;
				}				
			}			
			if(dibujos) addChild(dibujos);
			return(retorno);
		}	
		
		
		/*Esta funcion calcula la posición real del objeto en la pantalla, con el fin
		de poder pasar a coordenadas globales las coordenadas, y de esta manera utilizarlas
		para hacer calculos de distancia*/
		private function real_scr_pos(obj:DisplayObject3D,viewport:Viewport3D):Number3D{
			obj.autoCalcScreenCoords = true;
			var pos:Number3D = new Number3D(obj.screen.x + viewport.viewportWidth / 2, obj.screen.y + viewport.viewportHeight / 2,obj.screenZ);
			return pos;
		}



		/*
		Funcion que Permite saber si un Marker apunta a Otro Marker
		- m1 y m2 son los Markers (donde m1 apunta a m2). 
		- La parte de Abajo del Marker es la "punta".
		
		Esta funcion no se utiliza en este programa, pero es mucho mas simple
		a la de marker_apunta_objeto (aunque son similares), además en general
		esta funciona mucho mejor que la otra.
		*/
		private function Marker_apunta_Marker(m1:FLARMarker,m2:FLARMarker):Boolean{
			var corners:Vector.<Point>;
			var vertex:Point;			
			var i1:int=-1;
			var i2:int=-1;
			var direccion:int=-1;
			var c:int;
			var retorno:Boolean=false;						
			
			if(dibujos) dibujos.graphics.clear();
			dibujos = new Shape();			
			
			//Calculo de orientación del marker. Segun la rotación
			//del ejeZ se determina hacia que lado esta apuntando
			if((m1.rotationZ > 315 && m1.rotationZ < 360) || (m1.rotationZ <= 45 && m1.rotationZ >= 0)){
				direccion=0;//Arriba
			}
			if(m1.rotationZ > 225 && m1.rotationZ<= 315){
				direccion=1;//Izquierda
			}
			if(m1.rotationZ > 135 && m1.rotationZ<= 225){
				direccion=2;//Abajo
			}										
			if(m1.rotationZ > 45 && m1.rotationZ<= 130){
				direccion=3;//Derecha
			}
			
			/*
			Calculos de esquinas según la orientación
			Segun la orientación se calculan cuales 2 puntos
			se encuentran en esa dirección.
			*/
			corners = m1.corners;
			switch(direccion){
				case 0: //arriba	
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.y >= vertex.y){
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;							
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}					
					break;
				case 1: //izquierda
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.x >= vertex.x){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);					
						}
					}
					break;
				case 2: //abajo
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.y < vertex.y){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}
					break;
				case 3: //derecha
					for (c=0; c<corners.length; c++) {
						vertex = corners[c];
						if(m1.x < vertex.x){ 
							if(i1>=0 && i2<0) i2=c; 
							if(i1<0 && i2<0) i1=c;
							//dibujos.graphics.lineStyle(3, 0x00FF00);
							//dibujos.graphics.drawCircle(vertex.x, vertex.y, 5);								
						}
					}	
					break;					
			}

			//Si se han encontrado las 2 esquinas orientadas
			if (i1>=0 && i2>=0){			
				/*
				Calculo del punto medio entre las 2 esquinas seleccionadas.
				Si en la imagen se ve correctamente según la orientación
				el punto azul, entonces se detectó correctamente la orientación
				*/			
				var p3:Point=Point.interpolate(corners[i1],corners[i2],0.5);
				
				//Dibuja el punto p3
				dibujos.graphics.lineStyle(3, 0x0000FF);
				dibujos.graphics.drawCircle(p3.x, p3.y, 5);
				
				//Dibuja el punto central del marker1
				//dibujos.graphics.lineStyle(3, 0xFF0000);
				//dibujos.graphics.drawCircle(m1.x,m1.y,3);
				
				//Dibuja el punto central del marker2
				//dibujos.graphics.lineStyle(3, 0xFF0000);
				//dibujos.graphics.drawCircle(m2.x,m2.y,3);
				
				//Dibuja linea entre p3 y el marker2
				//dibujos.graphics.moveTo(p3.x,p3.y);
				//dibujos.graphics.lineTo(m2.x,m2.y);
				
				//dibuja linea entre el marker1 y el marker2
				//dibujos.graphics.lineStyle(3, 0xFFD700);
				//dibujos.graphics.lineTo(m1.x,m1.y);

				/*se calcula si p3 pertenece a la ecuacion de la recta que pasa por el marker1 y 
				el marker2. La idea es que esta diferencia sea la menor posible. Se 
				recomiendan valores entre 0 y 10. Si se cumple la condición, el marker1 si se 
				encuentra apuntando al marker2*/		
				var r:Number=Math.abs((m1.y+(m2.y - m1.y)*(p3.x - m1.x)/(m2.x - m1.x))-p3.y);
				//trace("r: " + r);
				if(r<5){
					retorno=true;
				}				
			}			
			if(dibujos) addChild(dibujos);
			return(retorno);
		}

        private function click(e:MouseEvent):void{
			var request:URLRequest = new URLRequest("http://ra-utem.uphero.com/demos.html");
			try {
			  navigateToURL(request, '_self');
			} catch (e:Error) {
			  trace("Error occurred!");
			}
        }
        
		private function onCameraStatus (evt:StatusEvent) :void {
			//this.fm.flarCameraSource.camera.removeEventListener(StatusEvent.STATUS, this.onCameraStatus);
			
			if (evt.code == "Camera.Muted") {				
				txt.x=5;
				txt.y=5;
				txt.autoSize=TextFieldAutoSize.LEFT;
			
			    var format:TextFormat	= new TextFormat();
		        format.size = 13;
		        format.color=0xF94607;
		        format.bold=true;
		        txt.defaultTextFormat=format;				
				
				txt.opaqueBackground=true;
				addChild(noCam); 
		        txt.text="ERROR: Debe permitir al programa acceder a su cámara web presionando el botón \"Permitir\" o \"Allow\".\nRecargue la página para que salga el mensaje otra vez.";     
			    addChild(txt);
			    
				//this.dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, true, false,
					//"Camera access denied by user.  If you wish to view this content, please right/ctrl-click here and click 'settings' to enable your camera, and then refresh this page."));
			}
		}        
			
	}
}